#1. dry-run
kubectl create deploy nginx-dr --image=nginx --dry-run=client -o yaml > nginx.yaml

#2. env
<...>
    spec:
      <...>
      env:
      - name: TEST
        value: "hello world 1"   
      - name: TEST2
        value: "hello world 2"
<...>

# env could be fetched from seted environments by k8s:
# metadata.name
# metadata.namespace
# metadata.lables
# metadata.annotations
# spec.nodeName
# spec.serviceAccountName
# status.hostIP
# status.podIP
# examole:
<...>
  spec:
    <...>
    env:
    - name: MY_NODENAME
      valueFrom:
        fieldRef:
          fieldPath: spec.nodeName
    - name: MY_POD_NAMESPACE
      calueFrom:
        fieldRef:
          fieldPath: metadata.namespace
    - name: MY_POD_SERVICE_ACCOUNT
      valueFrom:
        fieldRef:
          fieldPath: status.podIP
    <...>
  <...>

#3. describe
k describe pod -n default <pod_name>

#4. commands and args (how k8s' commands and args overrides the containers' ones)
image_enterypoint  | image_cmd  | container command  | container_args |  command_run
    [/ep-1]        | [foo bar]  |         --         |       --       | [/ep-1 foo bar]
    [/ep-1]        | [foo bar]  |      [/ep-2]       |       --       | [/ep-2]
    [/ep-1]        | [foo bar]  |         --         |    [zoo boo]   | [/ep-1 zoo boo]
    [/ep-1]        | [foo bar]  |      [/ep-2]       |    [zoo boo]   | [/ep-2 zoo boo]

#5. scheduling
#5.1 node selector
  <...>
    spec:
      <...>
      template:
        <...>
        spec:
          <...>
          nodeSelector:
            type: cdc

  #5.2 node afinity
    PidSpec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:   # even one match of nodeSelectorTerms brings deploy
            - matchExpressions:  # but if there are matchExpressions, all of them
              - key: kubernetes.io/e2e-az-name                 # shouled be matched
              operator: In
              values:
              - e2e-az1
              - e2e-az2
              preferredDuringSchedulingIgnoredDuringExecution:
              - weight: 1
              preference:
                matchExpressions:
                - key: another-node-label-key
                  operator: In
                  values:
                  - another-node-label-value

#5.3 Inter-pod affinity
podAffinity: # all pods on all nodes
  requiredDuringSchedulingIgnoredDuringExecution:
    - labelSelector:
        matchExpressions:
        - key: security
          operator: In
          values:
          - S1
      topologyKey: failure-domain.beta.kubernetes.io/zone

podAntiAffinity: # all pods on one node
  preferredDuringSchedulingIgnoredDuringExecution:
    - weight: 100
      podAffinityTerm:
        labelSelector:
          matchExpressions:
          - key: security
            operator: In
            values:
            - S2
        topologyKey: kubernetes.io/hostname

#5.4 Taints and Toleration
